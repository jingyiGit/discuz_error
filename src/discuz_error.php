<?php

class _discuz_error
{
  private static $config = [
    'showType' => 'discuz',
  ];
  private static $discuz_abnormal = false;
  private static $discuz_abnormal_type = 'error';
  
  /**
   * 初始化
   */
  public static function init()
  {
    register_shutdown_function(['_discuz_error', 'initDatabase']);
  }
  
  /**
   * 初始化数据库
   */
  private static function initDatabase()
  {
    if (!DB::result_first("SHOW TABLES LIKE 'pre_jerror_logs'")) {
      $sql = <<<EOF
CREATE TABLE `pre_jerror_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `method` varchar(6) DEFAULT '' COMMENT 'GET POST PUT等',
  `type` varchar(20) NOT NULL DEFAULT '' COMMENT '错误类型，如：system、db',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '当前请求的url',
  `content_type` varchar(50) DEFAULT '' COMMENT '内容类型，如：multipart/form-data;',
  `headers` text COMMENT 'cookies',
  `content` longtext NOT NULL COMMENT '自定义内容',
  `get` text NOT NULL COMMENT '当前请求的$_GET 数据',
  `post` text NOT NULL COMMENT '当前请求的$_POST 数据',
  `post_data` longtext NOT NULL COMMENT '通过 file_get_contents("php://input") 获取',
  `batch_number` char(32) NOT NULL COMMENT '日志批号',
  `updated_at` int(10) NOT NULL DEFAULT '0',
  `created_at` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `batch_number` (`batch_number`) USING BTREE,
  KEY `type` (`type`),
  KEY `created_at` (`created_at`),
  KEY `url` (`url`),
  KEY `method` (`method`),
  KEY `content_type` (`content_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='错误日志收集表';
EOF;
      DB::query($sql);
      echo '初始化成功';
    } else {
      echo '已初始化过了';
    }
  }
  
  public static function setConfig($config = [])
  {
    self::$config = array_merge(self::$config, $config);
  }
  
  private static function write_sys_error($type = '', $content = '')
  {
    // dd($_SERVER);
    $data = array_merge(self::getBasicInfo(), [
      'type'    => $type,
      'content' => $content,
    ]);
    DB::insert('jerror_logs', $data);
    return $data['batch_number'];
  }
  
  /**
   * 获取基本的一些信息
   */
  private static function getBasicInfo($url = '', $method = false)
  {
    $content_type = $_SERVER['HTTP_CONTENT_TYPE'] ?? '';
    if (stripos($content_type, 'boundary=') !== false) {
      $content_type = strstr($content_type, 'boundary=', true);
    }
    return [
      'url'          => $url ?: ('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']),
      'method'       => $method ?: $_SERVER['REQUEST_METHOD'],
      'content_type' => $content_type,
      'get'          => json_encode($_GET, true),
      'post'         => json_encode($_POST, true),
      'post_data'    => file_get_contents('php://input'),
      'headers'      => json_encode(apache_request_headers(), JSON_UNESCAPED_UNICODE),
      'batch_number' => self::uuid(''),
      'updated_at'   => time(),
      'created_at'   => time(),
    ];
  }
  
  private static function getRequestType()
  {
    // AJAX请求(返回JSON格式)
    if (isset($_SERVER['HTTP_ACCEPT']) && stripos($_SERVER['HTTP_ACCEPT'], 'application/json') !== false) {
      return 'ajax';
      
    } else if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      return 'ajax';
      
      // 正常请求(返回HTML格式)
    } else {
      return 'html';
    }
  }
  
  /**
   * 目前没用到
   *
   * @param $errno
   * @param $errstr
   * @param $errfile
   * @param $errline
   */
  public static function handleError($errno, $errstr, $errfile, $errline)
  {
    if (in_array($errno, [1, 16, 64, 256, 4096])) {
      self::write_sys_error('error', $errstr);
      discuz_error::system_error($errstr, false, true, false);
    }
  }
  
  /**
   * 处理异常回调
   *
   * @param $exception
   */
  public static function handleException($exception)
  {
    self::$discuz_abnormal = true;
    if ($exception instanceof DbException) {
      self::$discuz_abnormal_type = 'db';
    } else {
      self::$discuz_abnormal_type = 'system';
    }
    discuz_error::exception_error($exception);
  }
  
  /**
   * 处理错误记录
   */
  public static function handleShutdown()
  {
    $error = error_get_last();
    if (self::$discuz_abnormal || ($error && in_array($error['type'], [1, 16, 64, 256, 4096]))) {
      self::$discuz_abnormal = false;
      self::write_sys_error(self::$discuz_abnormal_type, ob_get_contents());
      self::showErrorInfo();
    }
    if ($error && $error['type'] & DISCUZ_CORE_DEBUG) {
      discuz_error::system_error($error['message'], false, true, false);
    }
  }
  
  private static function showErrorInfo()
  {
    // 不是使用discuz默认的显示方式
    if (self::$config['showType'] != 'discuz') {
      // 清空缓冲区内容
      ob_clean();
      
      // 是否显示自定义错误
      // 文件方式
      if (file_exists(self::$config['showType'])) {
        include self::$config['showType'];
        
        // 内容方式
      } else if (self::$config['showType']) {
        echo self::$config['showType'];
      }
    } else {
      // 默认显示Discuz的，不处理即可
    }
  }
  
  /**
   * 生成uuid
   *
   * @param string $separate 分隔符
   * @return string
   */
  private static function uuid($separate = '-')
  {
    $chars = md5(uniqid(mt_rand(), true));
    return substr($chars, 0, 8) . $separate
           . substr($chars, 8, 4) . $separate
           . substr($chars, 12, 4) . $separate
           . substr($chars, 16, 4) . $separate
           . substr($chars, 20, 12);
  }
}

// 首次，手动初始化数据库
// 任意的访问地址 + diy_discuz_error_init=1
if (isset($_GET['diy_discuz_error_init'])) {
  _discuz_error::init();
}
