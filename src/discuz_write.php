<?php

trait discuz_write
{
    private static function write_sys_error($type = '', $content = '')
    {
        $data = array_merge(self::getBasicInfo(), [
            'type'        => $type,
            'content'     => $content,
            'content_md5' => self::getErrorContent($content),
        ]);
        DB::insert('jerror_logs', $data);
        
        // 删除过期记录
        $del_day = date('Ymd', time());
        DB::delete('jerror_logs', ['del_day' => $del_day], 20);
        
        $del_day = date('Ymd', time() - self::$config['retainDay'] * 86400);
        DB::query('DELETE FROM `pre_jerror_logs` WHERE `del_day` < ' . $del_day . ' limit 100');
        
        // 处理显示错误
        self::showErrorInfo($data['error_batch'], $content);
    }
    
    /**
     * 取出错误内容的md5
     *
     * @param $content
     * @return string
     */
    private static function getErrorContent($content)
    {
        if (preg_match("#<div class='info'>(.*?)<div class=\"sql\">#", $content, $res)) {
            return md5($res[1]);
            
        } elseif (preg_match("#<div class='info'>(.*?)</div>#", $content, $res)) {
            return md5($res[1]);
            
        } else {
            return md5($content);
        }
    }
    
    /**
     * 处理显示错误
     *
     * @param string $error_batch 错误批号
     */
    private static function showErrorInfo($error_batch, $content)
    {
        // 清空缓冲区内容
        ob_clean();
        
        // 显示Discuz
        if (self::$config['showType'] == 'discuz') {
            echo str_replace('由此给您带来的访问不便我们深感歉意.', '由此给您带来的访问不便我们深感歉意，error_batch: ' . $error_batch, $content);
            
            // 以JSON的方式输出
        } else if (self::$config['showType'] == 'json') {
            $data = [
                'status'      => 0,
                'msg'         => '系统错误',
                'error_time'  => date('Y-m-d H:i:s', time()),
                'error_batch' => $error_batch,
            ];
            header('Content-Type: application/json; charset=UTF-8');
            exit(json_encode($data, JSON_UNESCAPED_UNICODE));
            
            // 不是使用discuz默认的显示方式
        } else {
            // 是否显示自定义错误
            // 文件方式
            if (file_exists(self::$config['showType'])) {
                include self::$config['showType'];
                echo "<p style='text-align: center;'>error_batch: {$error_batch}</p>";
                
                // 内容方式
            } else if (self::$config['showType']) {
                echo self::$config['showType'];
                echo "<p style='text-align: center;'>error_batch: {$error_batch}</p>";
            }
        }
    }
    
    /**
     * 获取基本的一些信息
     */
    private static function getBasicInfo($url = '', $method = false)
    {
        $content_type = $_SERVER['HTTP_CONTENT_TYPE'] ?? '';
        if (stripos($content_type, 'boundary=') !== false) {
            $content_type = strstr($content_type, 'boundary=', true);
        }
        return [
            'url'          => $url ?: ('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']),
            'method'       => $method ?: $_SERVER['REQUEST_METHOD'],
            'content_type' => $content_type,
            'get'          => json_encode($_GET, true),
            'post'         => json_encode($_POST, true),
            'post_data'    => file_get_contents('php://input'),
            'headers'      => json_encode(apache_request_headers(), JSON_UNESCAPED_UNICODE),
            'error_batch'  => self::uuid(''),
            'del_day'      => date('Ymd', time() + self::$config['retainDay'] * 86400),
            'client_ip'    => $_SERVER['HTTP_X_FORWARDED_FOR'] ?? $_SERVER['HTTP_X_REAL_IP'] ?? $_SERVER['REMOTE_ADDR'],
            'created_at'   => time(),
        ];
    }
    
    /**
     * 请当前客户端请求的类型
     *
     * @return string
     */
    private static function getRequestType()
    {
        // AJAX请求(返回JSON格式)
        if (isset($_SERVER['HTTP_ACCEPT']) && stripos($_SERVER['HTTP_ACCEPT'], 'application/json') !== false) {
            return 'ajax';
            
        } else if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            return 'ajax';
            
            // 正常请求(返回HTML格式)
        } else {
            return 'html';
        }
    }
}
