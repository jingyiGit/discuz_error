<?php
// 初始化一些设置和数据库

trait discuz_init
{
  /**
   * 初始化
   */
  public static function init()
  {
    register_shutdown_function(['_discuz_error', 'initDatabase']);
  }
  
  /**
   * 初始化数据库
   */
  public static function initDatabase()
  {
    ob_clean();
    if (!DB::result_first("SHOW TABLES LIKE 'pre_jerror_logs'")) {
      $sql = <<<EOF
CREATE TABLE `pre_jerror_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `method` varchar(6) DEFAULT '' COMMENT 'GET POST PUT等',
  `type` varchar(20) NOT NULL DEFAULT '' COMMENT '错误类型，如：system、db',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '当前请求的url',
  `content_type` varchar(50) DEFAULT '' COMMENT '内容类型，如：multipart/form-data;',
  `headers` text COMMENT 'cookies',
  `content` longtext NOT NULL COMMENT '自定义内容',
  `content_md5` char(32) NOT NULL DEFAULT '' COMMENT 'content 全部内容取的md5，可有效的去除相同的报错',
  `get` text NOT NULL COMMENT '当前请求的Array 数据',
  `post` text NOT NULL COMMENT '当前请求的Array 数据',
  `post_data` longtext NOT NULL COMMENT '通过 file_get_contents("php://input") 获取',
  `error_batch` char(32) NOT NULL DEFAULT '' COMMENT '日志批号',
  `del_day` int(10) NOT NULL DEFAULT '0' COMMENT '待删除日期，如：20220610，当到了这个日期，记录将会被删除掉',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态：1=待解决，2=已解决',
  `client_ip` char(15) DEFAULT NULL COMMENT '客户IP',
  `solve_time` int(10) NOT NULL DEFAULT '0' COMMENT '解决时间',
  `created_at` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `batch_number` (`error_batch`) USING BTREE,
  KEY `type` (`type`),
  KEY `created_at` (`created_at`),
  KEY `url` (`url`),
  KEY `method` (`method`),
  KEY `content_type` (`content_type`),
  KEY `del_day` (`del_day`),
  KEY `status` (`status`),
  KEY `content_md5` (`content_md5`,`created_at`,`status`) USING BTREE,
  KEY `client_ip` (`client_ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='错误日志收集表';
EOF;
      DB::query($sql);
      echo '<h1 style="text-align: center;color: green;">初始化成功</h1>';
    } else {
      echo '<h1 style="text-align: center;color: red;">已初始化过了</h1>';
    }
  }
}
