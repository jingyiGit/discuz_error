<?php
// 查看日志

if (!isset($_GET['_action_'])) {
    return;
}

// 首页
if ($_GET['_action_'] == 'view') {
    register_shutdown_function('_index_');
    
    // 获取列表
} elseif ($_GET['_action_'] == 'all') {
    register_shutdown_function('_getList_');
    
    // 查看详情
} elseif ($_GET['_action_'] == 'viewContent') {
    register_shutdown_function('_viewContent_');
    
    // 更新记录状态
} elseif ($_GET['_action_'] == 'updateStatus') {
    register_shutdown_function('_updateStatus_');
}

function _checkPass_()
{
    if (!isset($_COOKIE['_p_'])) {
        return false;
    } elseif (_discuz_error::getViewPass() != $_COOKIE['_p_']) {
        return false;
    } else {
        return true;
    }
}

function _index_()
{
    if (!_checkPass_()) {
        return;
    }
    ob_clean();
    include 'html/index.htm';
}

function _updateStatus_()
{
    if (!_checkPass_()) {
        return;
    }
    ob_clean();
    if (DB::update('jerror_logs', ['status' => 2, 'solve_time' => time()], [
        'content_md5' => $_GET['content_md5'],
        'status'      => 1,
    ])) {
        _succ_('更新记录状态成功');
    } else {
        _fail_('更新记录状态失败');
    }
}

function _viewContent_()
{
    if (!_checkPass_()) {
        return;
    }
    $handleHeaders = function ($headers) {
        $headers = json_decode($headers, true);
        $list    = [];
        foreach ($headers as $k => $v) {
            $list[] = "{$k}: {$v}";
        }
        krsort($list);
        return implode("<br>\r\n", $list);
    };
    ob_clean();
    $row     = DB::fetch_first("SELECT * FROM pre_jerror_logs WHERE id=" . intval($_GET['_id']));
    $content = file_get_contents(DISCUZ_ROOT . 'vendor/ijingyi/discuz_error/src/html/view-content.htm');
    $content = str_replace('{method}', $row['method'], $content);
    $content = str_replace('{url}', $row['url'], $content);
    $content = str_replace('{content_type}', $row['content_type'] ?: '无', $content);
    $content = str_replace('{headers}', $handleHeaders($row['headers']), $content);
    $content = str_replace('{get}', $row['get'], $content);
    $content = str_replace('{post}', $row['post'], $content);
    $content = str_replace('{post_data}', $row['post_data'], $content);
    $content = str_replace('{content}', $row['content'] ?: '空内容', $content);
    $content = str_replace('{created_at}', date('Y-m-d H:i:s', $row['created_at']), $content);
    // 今天相同错误统计
    $start_time   = date('Y-m-d', time());
    $end_time     = strtotime($start_time . ' 23:59:59');
    $start_time   = strtotime($start_time . ' 00:00:00');
    $error1_count = (int)DB::result_first(
        "SELECT COUNT(*) FROM pre_jerror_logs" .
        " WHERE content_md5='{$row['content_md5']}' AND created_at BETWEEN {$start_time} AND {$end_time}"
    );
    $content      = str_replace('{error1_count}', $error1_count, $content);
    
    // 今天相同错误统计
    $start_time   = strtotime('-7 day', time());
    $end_time     = strtotime(date('Y-m-d', time()) . ' 23:59:59');
    $error7_count = (int)DB::result_first(
        "SELECT COUNT(*) FROM pre_jerror_logs" .
        " WHERE content_md5='{$row['content_md5']}' AND created_at BETWEEN {$start_time} AND {$end_time}"
    );
    $content      = str_replace('{error7_count}', $error7_count, $content);
    
    echo $content;
}

function _getList_()
{
    if (!_checkPass_()) {
        return;
    }
    ob_clean();
    $sql  = '';
    $_GET = dhtmlspecialchars($_GET);
    if ($status = trim($_GET['status'])) {
        $sql .= " AND status='{$status}'";
    }
    if ($type = trim($_GET['type'])) {
        $sql .= " AND type='{$type}'";
    }
    if ($method = trim($_GET['method'])) {
        $sql .= " AND method='{$method}'";
    }
    if ($content_md5 = trim($_GET['content_md5'])) {
        $sql .= " AND content_md5='{$content_md5}'";
    }
    if ($error_batch = trim($_GET['error_batch'])) {
        $sql .= " AND error_batch='{$error_batch}'";
    }
    if ($url = trim($_GET['url'])) {
        $sql .= " AND url like '%{$url}%'";
    }
    $sql = "SELECT id,method,status,type,url,content_type,content_md5,error_batch,client_ip,solve_time,created_at FROM pre_jerror_logs" .
           " WHERE 1{$sql}" .
           " ORDER BY status ASC,id DESC";
    _laydata_($sql, function (&$data) {
        $date = date('Ymd', time());
        foreach ($data as &$v) {
            if ($v['solve_time']) {
                if (date('Ymd', $v['solve_time']) == $date) {
                    $v['solve_time'] = date('H:i:s', $v['solve_time']);
                } else {
                    $v['solve_time'] = date('m-d H:i:s', $v['solve_time']);
                }
            } else {
                $v['solve_time'] = '-';
            }
            
            if (date('Ymd', $v['created_at']) == $date) {
                $v['created_at'] = date('H:i:s', $v['created_at']);
            } else {
                $v['created_at'] = date('m-d H:i:s', $v['created_at']);
            }
        }
    });
}

/**
 * 自主分页函数
 *
 * @param string|array   $sql
 * @param Closure|string $cb
 * @param int            $cacheTime 缓存数据，0=不缓存，大于0时，表示缓存时长(秒)
 * @param string         $countSql  统计行数的SQL，一般用于数据量太大，分页
 * @throws DoNotRushBackJSONException
 */
function _laydata_($sql, $cb = null, $cacheTime = 0, $countSql = false)
{
    $time        = microtime(true);
    $var         = ['code' => 0, 'status' => 1, 'msg' => ''];
    $var['data'] = _paginate_(function ($start, $limit) use ($sql, &$var) {
        $var['per'] = $limit;
        if (stripos($sql, '{limit}') !== false) {
            $sql = str_replace('{limit}', "limit $start, $limit", $sql);
        } else {
            $sql .= " limit $start, $limit";
        }
        return DB::fetch_all($sql);
    });
    
    if ($countSql) {
        $var['count'] = DB::result_first($countSql);
    } else if (stripos($sql, '{limit}') !== false) {
        $sql_count    = str_replace('{limit}', "", $sql);
        $sql_count    = preg_replace('#ORDER BY .*? DESC#i', "", $sql_count);
        $sql_count    = preg_replace('#ORDER BY .*? ASC#i', "", $sql_count);
        $sql_count    = preg_replace('#ORDER BY [a-zA-Z]\w+#i', "", $sql_count);
        $var['count'] = DB::result_first(preg_replace('#^select\s+([\s\S]+?)\s+from\s+([a-zA-Z0-9_]+\.)?pre_#i', 'select count(*) from $2pre_', $sql_count));
    } else {
        $sql_count    = $sql;
        $var['count'] = DB::result_first(preg_replace('#^select\s+([\s\S]+?)\s+from\s+([a-zA-Z0-9_]+\.)?pre_#i', 'select count(*) from $2pre_', $sql_count));
    }
    
    if ($cb) {
        $cb($var['data'], $var['count'], $var['per']);
    }
    $var['execute_duration'] = round(microtime(true) - $time, 3);
    header('Content-Type: application/json; charset=UTF-8');
    exit(json_encode($var, JSON_UNESCAPED_UNICODE));
}

/**
 * 分页函数
 *
 * @param Closure $cb
 * @return mixed
 */
function _paginate_(Closure $cb)
{
    $page  = $_GET['page'] > 0 ? intval($_GET['page']) : 1;
    $limit = hasParam('limit') && $_GET['limit'] >= 0 ? intval($_GET['limit']) : 20;
    
    return $cb(($page - 1) * $limit, $limit, $page);
}

/**
 * 成功
 *
 * @param mixed $data
 */
function _succ_($data = null)
{
    if (is_null($data)) {
        _json_(['status' => 1]);
    }
    _json_(['status' => 1, 'data' => $data]);
}

/**
 * 失败
 *
 * @param string $msg
 * @param int    $status
 */
function _fail_($msg, $status = 0)
{
    if (is_array($msg)) {
        _json_($msg + ['status' => $status]);
    }
    _json_(['status' => $status, 'msg' => $msg]);
}

function _json_($value)
{
    header('Content-Type: application/json; charset=UTF-8');
    exit(json_encode($value, JSON_UNESCAPED_UNICODE));
}
